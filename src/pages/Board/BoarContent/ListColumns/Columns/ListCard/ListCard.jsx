import React from 'react'
import Box from '@mui/material/Box'
import TrelloCard from './Card/Card'
import {
  SortableContext,
  verticalListSortingStrategy
} from '@dnd-kit/sortable'

const ListCard = ({ ListCard }) => {
  return (
    <>
      <SortableContext
        items={ListCard?.map((c) => c._id)}
        strategy={verticalListSortingStrategy}
      >
        <Box
          sx={{
            p: '5px',
            m: '5px',
            display: 'flex',
            flexDirection: 'column',
            gap: 1,
            overflowX: 'hidden',
            overflowY: 'auto',
            maxHeight: (theme) =>
              `calc(${theme.trelloCustom.boardContentHeight} - ${theme.spacing(
                5
              )} - ${theme.trelloCustom.columnHeaderHeight} - ${
                theme.trelloCustom.columnFooterHeight
              })`,
            '&::-webkit-scrollbar-thumb': {
              background: '#ced0da',
              borderRadius: '8px'
            },
            '&::-webkit-scrollbar-thumb:hover': {
              background: '#dfc2cf'
            }
          }}
        >
          {ListCard.map((card, index) => (
            <TrelloCard card={card} key={index} />
          ))}
        </Box>
      </SortableContext>
    </>
  )
}

export default ListCard
