import Box from '@mui/material/Box'
import Chip from '@mui/material/Chip'
import DashboardIcon from '@mui/icons-material/Dashboard'
import PublicIcon from '@mui/icons-material/Public'
import AddToDriveIcon from '@mui/icons-material/AddToDrive'
import Avatar from '@mui/material/Avatar'
import AvatarGroup from '@mui/material/AvatarGroup'
import Tooltip from '@mui/material/Tooltip'
import Button from '@mui/material/Button'
import PersonAddAltIcon from '@mui/icons-material/PersonAddAlt'

const BoardBar = () => {
  return (
    <>
      <Box
        sx={{
          width: '100%',
          height: (theme) => theme.trelloCustom.boardBarHeight,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          padding: '0 20px',
          bgcolor: (theme) =>
            theme.palette.mode === 'dark' ? '#2f4860' : '#2d80d8',

          overflowX: 'auto',
          overflowY: 'hidden'
        }}
      >
        <Box sx={{ display: 'flex', alignItems: 'center', gap: '0 10px' }}>
          <Chip
            icon={<DashboardIcon />}
            label="Dashboard"
            clickable
            sx={{
              color: 'white',
              bgcolor: 'transparent',
              borderRadius: '4px',
              '& .MuiSvgIcon-root': { color: 'white' },
              '&:hover': {
                bgcolor: 'primary.50'
              }
            }}
          />
          <Chip
            icon={<PublicIcon />}
            label="Public Workspace"
            clickable
            sx={{
              color: 'white',
              bgcolor: 'transparent',
              borderRadius: '4px',
              '& .MuiSvgIcon-root': { color: 'white' },
              '&:hover': {
                bg: 'primary.50',
                color: 'white'
              }
            }}
          />
          <Chip
            icon={<AddToDriveIcon />}
            label="Add Drive"
            clickable
            sx={{
              color: 'white',
              bgcolor: 'transparent',
              borderRadius: '4px',
              '.MuiSvgIcon-root': { color: 'white' },
              '&:hover': {
                bgcolor: 'primary.50'
              }
            }}
          />
        </Box>

        <Box sx={{ display: 'flex', alignItems: 'center', gap: '0 10px' }}>
          <Button
            variant="outlined"
            startIcon={<PersonAddAltIcon />}
            sx={{
              color: 'white',
              border: '1px solid white',
              '&:hover': { border: '1px solid white' }
            }}
          >
            Create
          </Button>
          <AvatarGroup
            total={8}
            sx={{
              '& .MuiAvatar-root': {
                width: 32,
                height: 32,
                fontSize: '16px',
                color: 'white'
              }
            }}
          >
            <Tooltip title="Remy Sharp">
              <Avatar
                alt="Remy Sharp"
                src="https://img.freepik.com/free-photo/portrait-dark-skinned-cheerful-woman-with-curly-hair-touches-chin-gently-laughs-happily-enjoys-day-off-feels-happy-enthusiastic-hears-something-positive-wears-casual-blue-turtleneck_273609-43443.jpg"
                sx={{ width: 32, height: 32, color: 'white' }}
              />
            </Tooltip>
            <Tooltip title="Travis Howard">
              <Avatar
                alt="Travis Howard"
                src="https://www.google.com/url?sa=i&url=https%3A%2F%2Fstock.adobe.com%2Fsearch%3Fk%3Dpeople&psig=AOvVaw2VZrfWrEHZksD2xGcZmeHx&ust=1703250558771000&source=images&cd=vfe&ved=0CBEQjRxqFwoTCKjB2fXMoIMDFQAAAAAdAAAAABAS"
                sx={{ width: 32, height: 32, color: 'white' }}
              />
            </Tooltip>
            <Tooltip title="Agnes Walker">
              <Avatar
                alt="Agnes Walker"
                src="https://thumbs.dreamstime.com/b/young-african-american-girl-holding-lightbulb-inspiration-idea-looking-camera-blowing-kiss-being-lovely-sexy-224048778.jpg"
                sx={{ width: 32, height: 32, color: 'white' }}
              />
            </Tooltip>
            <Tooltip title="Trevor Henderson">
              <Avatar
                alt="Trevor Henderson"
                src="https://t3.ftcdn.net/jpg/03/18/04/12/360_F_318041202_dSFWSp38bnmiNxhLQnJDgnszmCoW7W0a.jpg"
                sx={{ width: 32, height: 32, color: 'white' }}
              />
            </Tooltip>
          </AvatarGroup>
        </Box>
      </Box>
    </>
  )
}

export default BoardBar
