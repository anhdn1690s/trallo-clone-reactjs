import Container from '@mui/material/Container'
import AppBar from '~/components/AppBar/AppBar'
import BoardBar from './BoardBar/BoardBar'
import BoardContent from './BoarContent/BoarContent'
import { mockData } from '~/apis/mock-data'

const Board = () => {
  return (
    <Container
      disableGutters
      maxWidth={false}
      sx={{ height: '100vh' }}
    >
      <AppBar />
      <BoardBar />
      <BoardContent dataBoard={mockData?.board}/>
    </Container>
  )
}

export default Board
