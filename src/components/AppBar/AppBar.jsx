import Box from '@mui/material/Box'
import SelectMode from '../SelectMode/SelectMode'
import AppsIcon from '@mui/icons-material/Apps'
import Typography from '@mui/material/Typography'
import Workspaces from './Menus/workspaces'
import Recent from './Menus/recent'
import Badge from '@mui/material/Badge'
import NotificationsIcon from '@mui/icons-material/Notifications'
import Tooltip from '@mui/material/Tooltip'
import Profile from './Menus/profile'

const AppBar = () => {
  return (
    <>
      <Box
        sx={{
          width: '100%',
          height: (theme) => theme.trelloCustom.appBarHeight,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          padding: '0 20px',
          bgcolor:(theme) => (theme.palette.mode ==='dark' ? '#2c3e50' : '#1565c0')
        }}
      >
        <Box sx={{ display: 'flex', alignItems: 'center', gap: '0 10px' }}>
          <AppsIcon sx={{ color: 'white' }} />
          <Typography
            variant="span"
            sx={{ fontSize: '21px', fontWeight: '900', color: 'white' }}
          >
            Trello
          </Typography>
          <Box>
            <Workspaces />
          </Box>
          <Box>
            <Recent />
          </Box>
        </Box>
        <Box sx={{ display: 'flex', alignItems: 'center', gap: '0 10px' }}>
          <Tooltip title="Notifications">
            <Badge color="secondary" variant="dot">
              <NotificationsIcon sx={{ color: 'white' }}/>
            </Badge>
          </Tooltip>
          <Profile />
          <SelectMode />
        </Box>
      </Box>
    </>
  )
}

export default AppBar
